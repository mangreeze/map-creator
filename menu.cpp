#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include "menu.h"
#include "Macro.h"
#include "createProject.h"

Menu::Menu(QWidget *parent)
    : QWidget(parent)
{
    this->setPositionWidget();
    this->setConnection();
    this->setFixedSize(800, 600);
    this->show();
}

Menu::~Menu()
{}

void Menu::fillListProject()
{
    QDir dir("./Project");
    if (dir.exists()) {
        QFileInfoList list = dir.entryInfoList();
        for (int ct = 0; ct < list.size(); ++ct) {
            if (list[ct].baseName().compare("") != 0) {
                m_listProject->addItem(list[ct].baseName());
            }
        }
    } else {
        dir.mkdir(".");
    }
}

void Menu::setPositionWidget()
{
    m_listProject = new QListWidget(this);
    m_listProject->setFixedSize(400, 250);
    m_listProject->move(200, 250);
    this->fillListProject();
    m_buttonCreateNewProject = new QPushButton("Créer", this);
    m_buttonCreateNewProject->setFixedSize(110, 30);
    m_buttonCreateNewProject->move(200, 520);
    m_buttonModifyProject = new QPushButton("Modifier", this);
    m_buttonModifyProject->setFixedSize(110, 30);
    m_buttonModifyProject->move(345, 520);
    m_buttonEraseProject = new QPushButton("Effacer", this);
    m_buttonEraseProject->setFixedSize(110, 30);
    m_buttonEraseProject->move(490, 520);

}

void Menu::setConnection()
{
    connect(m_listProject, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(SLOT_LaunchProject(QListWidgetItem *)));
    connect(m_buttonCreateNewProject, SIGNAL(clicked()), this, SLOT(SLOT_CreateNewProject()));
    connect(m_buttonModifyProject, SIGNAL(clicked()), this, SLOT(SLOT_ModifyProject()));
    connect(m_buttonEraseProject, SIGNAL(clicked()), this, SLOT(SLOT_EraseProject()));
}

bool Menu::verifyProjectInformation(QString std_fileName, QString *p_projectName, int *p_position)
{
    QFile file(std_fileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QString line = file.readAll();
        QJsonDocument documentJson = QJsonDocument::fromJson(line.toUtf8());
        QJsonObject objectJSON = documentJson.object();
        *p_projectName = objectJSON.value(PROJECT_NAME).toString();
        *p_position = objectJSON.value(PROJECT_HINT).toInt();
        file.close();
        if (*p_position > -1 && *p_position < 3) {
            return true;
        }
    }
    return false;
}

void Menu::eraseProject(QString str_nameDirectory, QList<QString> *error_file, QList<QString> *error_dir)
{
    QDir dir(str_nameDirectory);
    if (dir.exists()) {
        QFileInfoList list = dir.entryInfoList();
        for (int ct = 0; ct < list.size(); ct += 1) {
            QFileInfo fileInfo = list.at(ct);
            if (fileInfo.fileName()[0] != '.') {
                if (fileInfo.isDir()) {
                    this->eraseProject(str_nameDirectory + "/" + fileInfo.fileName(), error_file, error_dir);
                } else {
                    QFile file(str_nameDirectory + "/" + fileInfo.fileName());
                    if (file.remove() == false) {
                        error_file->append(file.fileName());
                    }
                }
            }
        }
        if (dir.rmdir(".") == false) {
            error_dir->append(dir.dirName());
        }
    }
}

void Menu::SLOT_LaunchProject(QListWidgetItem *nameProject)
{
    emit SIG_LaunchEditor(nameProject->text(), false);
}

void Menu::SLOT_CreateNewProject()
{
    emit SIG_CreateNewProject();
}

void Menu::SLOT_ModifyProject()
{
    QListWidgetItem *tmp = m_listProject->currentItem();
    if (tmp != nullptr)
        emit SIG_ModifyProject(tmp->text());
}

void Menu::SLOT_EraseProject()
{
     QListWidgetItem *tmp = m_listProject->currentItem();
     QList<QString> error_file;
     QList<QString> error_dir;

     if (tmp == nullptr)
         return;
     this->eraseProject("./Project/" + tmp->text(), &error_file, &error_dir);
     delete tmp;
     QString tmp_file;
     QString tmp_dir;

     for (int ct = 0; ct != error_file.size(); ct++) {
         tmp_file += error_file[ct] + "\n";
     }
     for (int ct = 0; ct != error_dir.size(); ct++) {
         tmp_dir += error_dir[ct] + "\n";
     }
     if (error_file.size() != 0) {
         QMessageBox::warning(this, "Erreur", "Les fichiers suivants n'ont pas put être effacé : \n" + tmp_file);
     } else if (error_dir.size() != 0) {
         QMessageBox::warning(this, "Erreur", "Les dossiers suivants n'ont pas put être effacé : \n" + tmp_dir);
     }
}
