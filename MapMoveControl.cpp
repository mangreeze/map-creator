#include <QtMath>
#include <QDebug>
#include "MapMoveControl.h"

MapMoveControl::MapMoveControl(sf::RenderWindow *widget, Graphics *graphics, sf::View *view)
    :   m_sfmlWidget(widget), m_graphics(graphics), m_view(view)
{}

void MapMoveControl::LookEvent()
{
    sf::Vector2i posMouse = sf::Mouse::getPosition(*m_sfmlWidget);

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && isMouseInWindow(posMouse)) {
        m_graphics->SetOneTileTexture(posMouse);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up)) {
        if (isViewNeedToMove(sf::Vector2f(0, -0.5))) {
            m_view->move(0, -0.5);
            m_sfmlWidget->setView(*m_view);
            m_graphics->setInfoWindow(*m_view, m_sfmlWidget->getSize());
            m_graphics->CreateVertices();
        }
    } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down)) {
        if (isViewNeedToMove(sf::Vector2f(0, 0.5))) {
            m_view->move(0, 0.5);
            m_sfmlWidget->setView(*m_view);
            m_graphics->setInfoWindow(*m_view, m_sfmlWidget->getSize());
            m_graphics->CreateVertices();
        }
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left)) {
        if (isViewNeedToMove(sf::Vector2f(-0.5, 0))) {
            m_view->move(-0.5, 0);
            m_sfmlWidget->setView(*m_view);
            m_graphics->setInfoWindow(*m_view, m_sfmlWidget->getSize());
            m_graphics->CreateVertices();
        }
    } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right)) {
        if (isViewNeedToMove(sf::Vector2f(0.5, 0))) {
            m_view->move(0.5, 0);
            m_sfmlWidget->setView(*m_view);
            m_graphics->setInfoWindow(*m_view, m_sfmlWidget->getSize());
            m_graphics->CreateVertices();
        }
    }
}

bool MapMoveControl::isMouseInWindow(sf::Vector2i posMouse)
{
    sf::Vector2u sizeWindow = m_sfmlWidget->getSize();

    if (posMouse.x > 0 && posMouse.x < (int)sizeWindow.x && posMouse.y > 0 && posMouse.y < (int)sizeWindow.y) {
        return true;
    } else {
        return false;
    }
}

bool MapMoveControl::isViewNeedToMove(sf::Vector2f posToMove)
{
    sf::Vector2f sizeView = m_view->getSize();
    sf::Vector2f sizeMax = m_graphics->CalculateSizePixelMap();
    sf::Vector2f posView;
    posView.x = m_view->getCenter().x - (sizeView.x / 2.);
    posView.y = m_view->getCenter().y - (sizeView.y / 2.);

    if (posToMove.x == 0) {
        if (posToMove.y == -0.5) {
            if (posView.y + posToMove.y < 0)
                return false;
            else
                return true;
        } else if (posView.y + posToMove.y + sizeView.y > sizeMax.x) {
            return false;
        } else {
            return true;
        }
    } else {
        if (posToMove.x == -0.5) {
            if (posView.x + posToMove.x < 0)
                return false;
            else
                return true;
        } else if (posView.x + posToMove.x + sizeView.x > sizeMax.y) {
            return false;
        } else {
            return true;
        }
    }
}
