#ifndef MENU_H
#define MENU_H

#include <QWidget>
#include <QPushButton>
#include <QListWidgetItem>
#include "createProject.h"

class Menu : public QWidget
{
    Q_OBJECT

public:
    Menu(QWidget *parent = nullptr);
    ~Menu();

public slots:
    void SLOT_LaunchProject(QListWidgetItem*);
    void SLOT_CreateNewProject(void);
    void SLOT_ModifyProject(void);
    void SLOT_EraseProject(void);

signals:
    void SIG_LaunchEditor(QString, bool);
    void SIG_CreateNewProject();
    void SIG_ModifyProject(QString);

private:
    void fillListProject();
    void setPositionWidget();
    void setConnection();
    bool verifyProjectInformation(QString std_fileName, QString *p_projetName, int *position);
    void eraseProject(QString str_nameDirectory, QList<QString> *error_file, QList<QString> *error_dir);

private:
    QListWidget *m_listProject;
    QPushButton *m_buttonCreateNewProject;
    QPushButton *m_buttonModifyProject;
    QPushButton *m_buttonEraseProject;
};

#endif // MENU_H
