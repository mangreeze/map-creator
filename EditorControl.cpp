#include "MapSizeControl.h"
#include "Macro.h"
#include "EditorControl.h"

EditorControl::EditorControl(QWidget *parent)
    :   QWidget(parent)
{
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
    updateGeometry();
    m_listTexture = new QListWidget(this);
    m_listHitbox = new QListWidget(this);
    m_buttonSave = new QPushButton("Sauvegarder");
    m_mapSizeWidget = new MapSizeControl();
    m_tabWidget = new QTabWidget();
    m_tabWidget->addTab(m_listTexture, "Texture");
    m_tabWidget->addTab(m_listHitbox, "Hitbox");
    setWindowLayout();
    createListHitbox();
    connect(m_listTexture, SIGNAL(currentRowChanged(int)), this, SLOT(ListTextureChange(int)));
    connect(m_listHitbox, SIGNAL(currentRowChanged(int)), this, SLOT(SLOT_listHitboxChange(int)));
}

void EditorControl::createListHitbox()
{
    QListWidgetItem *tmp = new QListWidgetItem;
    tmp->setIcon(QIcon(":/Image/GreyImage.png"));
    tmp->setText("Neutral");
    m_listHitbox->addItem(tmp);
    QListWidgetItem *tmp2 = new QListWidgetItem;
    tmp2->setIcon(QIcon(":/Image/Red.png"));
    tmp2->setText("Forbidden");
    m_listHitbox->addItem(tmp2);
    QListWidgetItem *tmp3 = new QListWidgetItem;
    tmp3->setIcon(QIcon(":/Image/Green.png"));
    tmp3->setText("Autorized");
    m_listHitbox->addItem(tmp3);
}

void EditorControl::CreateListTexture(const QJsonArray &array)
{
    for (int ct = 0; ct != array.size(); ct++) {
        QListWidgetItem *tmp = new QListWidgetItem;
        tmp->setIcon(QIcon(array.at(ct)[PROJECT_NAME].toString()));
        QStringList tmpList = array.at(ct)[PROJECT_NAME].toString().split('/');
        tmpList = tmpList[tmpList.size() - 1].split('.');
        tmp->setText(tmpList[0]);
        m_listTexture->addItem(tmp);
        m_hintTexture.append(array.at(ct)[PROJECT_HINT].toInt());
    }
    m_listTexture->setCurrentRow(0);
}

QListWidget *EditorControl::GetListTextureWidget()
{
    return m_listTexture;
}

void EditorControl::ListTextureChange(int row)
{
    emit SIG_NewHintTexture(m_hintTexture[row]);
}

void EditorControl::SLOT_listHitboxChange(int row)
{
    emit SIG_NewHintHitbox(row);
}

void EditorControl::setWindowLayout()
{
    m_layout = new QGridLayout();

    m_layout->addWidget(m_tabWidget, 0, 0, 3, 0);
    m_layout->addWidget(m_buttonSave, 4, 0);
    m_layout->addWidget(m_mapSizeWidget, 5, 0);
    setLayout(m_layout);
}

QSize EditorControl::sizeHint() const
{
    return QSize(480, 400);
}

QSize EditorControl::minimumSizeHint() const
{
    return QSize(256, 192);
}

QPushButton *EditorControl::GetSaveButton()
{
    return m_buttonSave;
}

MapSizeControl *EditorControl::GetPtrMapSizeControler()
{
    return m_mapSizeWidget;
}

QTabWidget *EditorControl::GetTabWidget()
{
    return m_tabWidget;
}
