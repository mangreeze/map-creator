#include <iostream>
#include <SFML/Window.hpp>
#include "qsfmlcanvas.h"

QSFMLCanvas::QSFMLCanvas(QWidget *parent, const QPoint &position, const QSize &size, unsigned int frameTime)
    : QWidget(parent), m_initialized(false)
{
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_OpaquePaintEvent);
    setAttribute(Qt::WA_NoSystemBackground);
    setFocusPolicy(Qt::StrongFocus);
    move(position);
    resize(size);
    m_timer.setInterval(frameTime);
}

void QSFMLCanvas::showEvent(QShowEvent*)
{
    if (!m_initialized)
    {

        // On crée la fenêtre SFML avec l'identificateur du widget
        RenderWindow::create(reinterpret_cast<sf::WindowHandle>(winId()));


        // On laisse la classe dérivée s'initialiser si besoin
        OnInit();

        // On paramètre le timer de sorte qu'il génère un rafraîchissement à la fréquence souhaitée
        connect(&m_timer, SIGNAL(timeout()), this, SLOT(repaint()));
        m_timer.start();

        m_initialized = true;
    }
}

QPaintEngine* QSFMLCanvas::paintEngine() const
{
    return 0;
}

void QSFMLCanvas::paintEvent(QPaintEvent*)
{
    // On laisse la classe dérivée faire sa tambouille
    OnUpdate();

    // On rafraîchit le widget
    sf::RenderWindow::display();
}

