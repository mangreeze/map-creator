#include <QLabel>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include "createProject.h"
#include "Macro.h"

CreateProject::CreateProject(QWidget *parent)
    : Abstract_project_information(parent)
{
    this->show();
}

CreateProject::~CreateProject()
{}

void CreateProject::EraseTexture()
{
    QList<QListWidgetItem *> textureToErase = m_listWidgetTexture->selectedItems();
    for (int ct = 0; ct != textureToErase.size(); ct++) {
        m_listTexture.removeOne(textureToErase[ct]->text());
        delete textureToErase[ct];
    }
    this->show();
}

void CreateProject::CreateWindowAddTexture()
{
    QString fichier = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Images (*.png *.gif *.jpg *.jpeg)");
    if (fichier.compare("") != 0) {
        m_listTexture.append(fichier);
        m_listWidgetTexture->addItem(fichier);
        this->show();
    }
}

void CreateProject::PressButtonValidate()
{
    if (m_projectName->text().compare("") == 0) {
        QMessageBox::warning(this, "Erreur titre projet", "Merci de rentrer un nom pour votre projet");
    } else if (m_listTexture.size() == 0) {
        QMessageBox::warning(this, "Erreur texture", "Merci d'ajouter au moins une texture");
    } else {
        this->CreateNewProject();
    }
}

void CreateProject::CreateNewProject()
{
    QDir dir("");
    if (dir.mkdir("./Project/" + m_projectName->text()) == false) {
         QMessageBox::critical(this, "Doublon nom de projet", "Ce projet existe déja, veuillez modifier le titre avant de revalider");
    } else {
        QFile file("./Project/" + m_projectName->text() + "/Information.json");

        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QJsonDocument documentJson;
        QJsonObject objectJson;
        objectJson.insert(PROJECT_NAME, QJsonValue::fromVariant(m_projectName->text()));
        objectJson.insert(PROJECT_NBR_LINE, QJsonValue::fromVariant(m_nbrLine->value()));
        objectJson.insert(PROJECT_PIXEL_LINE, QJsonValue::fromVariant(m_pixelLine->value()));
        objectJson.insert(PROJECT_NBR_COLUMN, QJsonValue::fromVariant(m_nbrColumn->value()));
        objectJson.insert(PROJECT_PIXEL_COLUMN, QJsonValue::fromVariant(m_pixelColumn->value()));
        bool result = this->CreateNewDirectoryTexture();
        QJsonArray texture;
        QJsonObject tmp2;
        tmp2.insert(PROJECT_NAME, QJsonValue::fromVariant(":/Image/GreyImage.png"));
        tmp2.insert(PROJECT_HINT, QJsonValue::fromVariant(0));
        texture.append(tmp2);
        for (int ct = 0; ct != m_textureAdd.size(); ct++) {
            QJsonObject tmp;
            tmp.insert(PROJECT_NAME, QJsonValue::fromVariant(m_textureAdd[ct]));
            tmp.insert(PROJECT_HINT, QJsonValue::fromVariant(ct + 1));
            texture.append(tmp);
        }
        objectJson.insert(PROJECT_TEXTURE, texture);
        documentJson.setObject(objectJson);
        file.write(documentJson.toJson());
        file.close();
        if (result) {
            emit SIG_launchEditor(m_projectName->text(), true);
        } else {
            EraseProject();
            emit SIG_returnMainLoop();
        }
    }
}

bool CreateProject::CreateNewDirectoryTexture()
{
    m_textureAdd.clear();
    QList<QString> error;
    QDir dir("");
    dir.mkdir("./Project/" + m_projectName->text() + "/Texture");
    dir.mkdir("./Project/" + m_projectName->text() + "/Save");
    for (int ct = 0; ct != m_listTexture.size(); ct++) {
        QStringList tmp = m_listTexture[ct].split(QLatin1Char('/'));
        if (QFile::copy(m_listTexture[ct], "./Project/" + m_projectName->text() + "/Texture/" + tmp[tmp.size() - 1]) == false) {
            error.append(m_listTexture[ct]);
        } else {
            m_textureAdd.append("./Project/" + m_projectName->text() + "/Texture/" + tmp[tmp.size() - 1]);
        }
    }
    if (error.size() != 0) {
        QString tmp = "Impossible de copier les textures suivantes : \n";
        for (int ct = 0; ct != error.size(); ct++) {
            tmp += error[ct] + "\n";
        }
        QMessageBox::critical(this, "Erreur Texture", tmp);
        return false;
    }
    return true;
}

void CreateProject::EraseProject()
{
     QList<QString> error_file;
     QList<QString> error_dir;

     this->eraseProject("./Project/" + m_projectName->text(), &error_file, &error_dir);
     QString tmp_file;
     QString tmp_dir;

     for (int ct = 0; ct != error_file.size(); ct++) {
         tmp_file += error_file[ct] + "\n";
     }
     for (int ct = 0; ct != error_dir.size(); ct++) {
         tmp_dir += error_dir[ct] + "\n";
     }
     if (error_file.size() != 0) {
         QMessageBox::warning(this, "Erreur", "Les fichiers suivants n'ont pas put être effacé : \n" + tmp_file);
     } else if (error_dir.size() != 0) {
         QMessageBox::warning(this, "Erreur", "Les dossiers suivants n'ont pas put être effacé : \n" + tmp_dir);
     }
}

void CreateProject::eraseProject(QString str_nameDirectory, QList<QString> *error_file, QList<QString> *error_dir)
{
    QDir dir(str_nameDirectory);
    if (dir.exists()) {
        QFileInfoList list = dir.entryInfoList();
        for (int ct = 0; ct < list.size(); ct += 1) {
            QFileInfo fileInfo = list.at(ct);
            if (fileInfo.fileName()[0] != '.') {
                if (fileInfo.isDir()) {
                    this->eraseProject(str_nameDirectory + "/" + fileInfo.fileName(), error_file, error_dir);
                } else {
                    QFile file(str_nameDirectory + "/" + fileInfo.fileName());
                    if (file.remove() == false) {
                        error_file->append(file.fileName());
                    }
                }
            }
        }
        if (dir.rmdir(".") == false) {
            error_dir->append(dir.dirName());
        }
    }
}
