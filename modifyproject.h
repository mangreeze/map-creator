#ifndef MODIFYPROJECT_H
#define MODIFYPROJECT_H

#include <QJsonObject>
#include "abstract_project_information.h"

class ModifyProject : public Abstract_project_information
{
    Q_OBJECT

public:
    ModifyProject(QString nameProject, QWidget *parent = nullptr);

private:
    void getJsonInformationProject(QString numProject);
    void initWindow(void);
    void EraseFileTexture(void);
    void CreateQObjectTexture(void);
    bool AddTextureToDirectory(void);
    void AddNewTexture(QString, QJsonArray, QJsonArray *);
    void EraseTextureInMap(void);

public slots:
    void CreateWindowAddTexture() override;
    void EraseTexture() override;
    void PressButtonValidate() override;

private:
    QJsonObject m_objectJson;
    QList<QString> m_tmpListTextureToErase;
    QList<QString> m_textureAdd;
    QList<bool> m_hintNewTexture;
    QList<int> m_hintEraseTexture;
    int m_hintAddNewTexture;
};

#endif // MODIFYPROJECT_H
