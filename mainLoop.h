#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QMainWindow>
#include <QWidget>
#include "menu.h"
#include "createProject.h"
#include "editorwindow.h"
#include "modifyproject.h"

class MainLoop : QObject
{
    Q_OBJECT
public:
    MainLoop();

public slots:
    void createWindowNewProject();
    void createWindowSelectionProject(void);
    void createWindowEditor(QString nameProject, bool newProject);
    void createWindowModifyProject(QString);

private:
    QWidget *m_widget;
};

#endif // MAINWINDOW_H
