#include <QDebug>
#include <QFile>
#include <QDataStream>
#include "MapProjectManagement.h"

MapProjectManagement::MapProjectManagement()
{

}

void MapProjectManagement::CreateEmptyMap(const int &height, const int &width)
{
    for (int ct = 0; ct != height; ct++) {
        std::vector<int> tmp;
        for (int ct2 = 0; ct2 != width; ct2 += 1) {
            tmp.push_back(0);
        }
        m_arrayMapTexture.push_back(tmp);
        m_arrayMapHitbox.push_back(tmp);
    }
}

void MapProjectManagement::FillArrayMap(const QString &fileName)
{
    QFile file("./Project/" + fileName + "/Save/MapTexture.xyz");

    m_arrayMapTexture.clear();
    m_arrayMapHitbox.clear();
    if (!file.open(QIODevice::ReadOnly)) {
        return;
    } else {
        QDataStream in(&file);
        int height;
        int width;
        in >> height >> width;
        for (int ct = 0; ct != height; ct++) {
            std::vector<int> tmp;
            for (int ct2 = 0; ct2 != width; ct2++) {
                int i;
                in >> i;
                tmp.push_back(i);
            }
            m_arrayMapTexture.push_back(tmp);
        }
        file.close();
    }

    QFile file2("./Project/" + fileName + "/Save/MapHitbox.xyz");
    if (!file2.open(QIODevice::ReadOnly)) {
        return;
    } else {
        QDataStream in(&file2);
        int height;
        int width;
        in >> height >> width;
        for (int ct = 0; ct != height; ct++) {
            std::vector<int> tmp;
            for (int ct2 = 0; ct2 != width; ct2++) {
                int i;
                in >> i;
                tmp.push_back(i);
            }
            m_arrayMapHitbox.push_back(tmp);
        }
        file2.close();
    }
}

void MapProjectManagement::SaveArrayMap(const QString &fileName)
{
    QFile file("./Project/" + fileName + "/Save/MapTexture.xyz");

    if (!file.open(QIODevice::WriteOnly)) {
        return;
    } else {
        int height = m_arrayMapTexture.size();
        int width = m_arrayMapTexture[0].size();
        QDataStream out(&file);
        out << (quint32)height;
        out << (quint32)width;
        for (int ct = 0; ct != (int)m_arrayMapTexture.size(); ct++) {
            for (int ct2 = 0; ct2 != (int)m_arrayMapTexture[ct].size(); ct2++) {
                out << (quint32)m_arrayMapTexture[ct][ct2];
            }
        }
        file.close();
    }

    QFile file2("./Project/" + fileName + "/Save/MapHitbox.xyz");

    if (!file2.open(QIODevice::WriteOnly)) {
        return;
    } else {
        int height = m_arrayMapHitbox.size();
        int width = m_arrayMapHitbox[0].size();
        QDataStream out(&file2);
        out << (quint32)height;
        out << (quint32)width;
        for (int ct = 0; ct != (int)m_arrayMapHitbox.size(); ct++) {
            for (int ct2 = 0; ct2 != (int)m_arrayMapHitbox[ct].size(); ct2++) {
                out << (quint32)m_arrayMapHitbox[ct][ct2];
            }
        }
        file2.close();
    }
    emit SIG_SaveSizeMap(m_arrayMapTexture.size(), m_arrayMapTexture[0].size());
}

void MapProjectManagement::ModifyMapTexture(const int height, const int width, const int hint)
{
    m_arrayMapTexture[height][width] = hint;
}

void MapProjectManagement::ModifyMapHitbox(const int height, const int width, const int hint)
{
    m_arrayMapHitbox[height][width] = hint;
}

void MapProjectManagement::EraseHintTextureMap(const int texture)
{
    for (int ct = 0; ct != (int)m_arrayMapTexture.size(); ct++) {
        for (int ct2 = 0; ct2 != (int)m_arrayMapTexture[ct].size(); ct2++) {
            if (m_arrayMapTexture[ct][ct2] == texture)
                m_arrayMapTexture[ct][ct2] = 0;
        }
    }
}

int MapProjectManagement::GetHintTileTexture(const int y, const int x)
{
    return m_arrayMapTexture[y][x];
}

int MapProjectManagement::GetHintTileHitbox(const int y, const int x)
{
    return m_arrayMapHitbox[y][x];
}

void MapProjectManagement::printArray()
{
    for (int ct = 0; ct != (int)m_arrayMapTexture.size(); ct++) {
        for (int ct2 = 0; ct2 != (int)m_arrayMapTexture[ct].size(); ct2++) {
            std::cout << m_arrayMapTexture[ct][ct2] << " ";
        }
        std::cout << std::endl;
    }
}

void MapProjectManagement::SLOT_AddColumn()
{
    for (int ct = 0; ct != (int)m_arrayMapTexture.size(); ct++) {
        m_arrayMapTexture[ct].push_back(0);
        m_arrayMapHitbox[ct].push_back(0);
    }
    emit SIG_UpdateVertices(m_arrayMapTexture.size(), m_arrayMapTexture[0].size());
}

void MapProjectManagement::SLOT_AddLine()
{
    std::vector<int> tmp(m_arrayMapTexture[0].size(), 0);
    m_arrayMapTexture.push_back(tmp);
    m_arrayMapHitbox.push_back(tmp);

    emit SIG_UpdateVertices(m_arrayMapTexture.size(), m_arrayMapTexture[0].size());
}

void MapProjectManagement::SLOT_EraseColumn()
{
    if (m_arrayMapTexture[0].size() <= 1)
        return;
    for (int ct = 0; ct != (int)m_arrayMapTexture.size(); ct++) {
        m_arrayMapTexture[ct].erase(m_arrayMapTexture[ct].end() - 1);
        m_arrayMapHitbox[ct].erase(m_arrayMapHitbox[ct].end() - 1);
    }
    emit SIG_UpdateVertices(m_arrayMapTexture.size(), m_arrayMapTexture[0].size());
}

void MapProjectManagement::SLOT_EraseLine()
{
    if (m_arrayMapTexture.size() > 1) {
        m_arrayMapHitbox.erase(m_arrayMapHitbox.end() - 1);
        m_arrayMapTexture.erase(m_arrayMapTexture.end() - 1);
        emit SIG_UpdateVertices(m_arrayMapTexture.size(), m_arrayMapTexture[0].size());
    }
}
