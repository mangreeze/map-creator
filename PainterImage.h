#ifndef PAINTERIMAGE_H
#define PAINTERIMAGE_H

#include <QPainter>
#include <QImage>
#include <QSize>
#include <QJsonArray>

class PainterImage
{
public:
    PainterImage(QJsonArray array, int nbrPixelLine, int nbrPixelColumn);
    void SaveImage(QString fileName);
    QImage *GetImage();
    int GetNbrTextureByLine();

private:
    void CreateImage(void);

private:
    QJsonArray m_array;
    QImage *m_image;
    int m_nbrTileImage;
    QSize m_pixelTile;
    QPainter *m_painter;
};

#endif // PAINTERIMAGE_H
