#ifndef ABSTRACT_PROJECT_INFORMATION_H
#define ABSTRACT_PROJECT_INFORMATION_H

#include <QWidget>
#include <QObject>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QPushButton>
#include <QList>
#include <QListWidget>
#include <QSpinBox>

class Abstract_project_information : public QWidget
{
    Q_OBJECT

public:
    Abstract_project_information(QWidget *parent = nullptr);
    ~Abstract_project_information();

private:
    void CreateLayoutInformationProject(void);
    void CreateLayoutWidgetTexture(void);
    void CreateLayoutWindow(void);
    void CreateLayoutValidateCancelButton(void);

public slots:
    virtual void CreateWindowAddTexture(void) {};
    virtual void PressButtonValidate(void) {};
    virtual void EraseTexture(void) {};
    void PressButtonCancel(void);

signals:
    void SIG_returnMainLoop();

protected:
    QLineEdit *m_projectName;

    QSpinBox *m_nbrLine;
    QSpinBox *m_nbrColumn;
    QSpinBox *m_pixelLine;
    QSpinBox *m_pixelColumn;

    QVBoxLayout *m_layoutWindow;
    QGridLayout *m_layoutProjectName;
    QHBoxLayout *m_layoutButtonCancelValidate;
    QGridLayout *m_layoutTexture;

    QPushButton *m_buttonAddTexture;
    QPushButton *m_buttonEraseTexture;
    QPushButton *m_buttonValidate;
    QPushButton *m_buttonCancel;

    QList<QString> m_listTexture;
    QListWidget *m_listWidgetTexture;
};

#endif // ABSTRACT_PROJECT_INFORMATION_H
