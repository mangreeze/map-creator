#ifndef EDITORWINDOW_H
#define EDITORWINDOW_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QWidget>
#include <QGridLayout>
#include "sfmlWidget.h"
#include "EditorControl.h"
#include "MapProjectManagement.h"

class EditorWindow : public QWidget
{
    Q_OBJECT

public:
    EditorWindow(QString nameProject, bool newProject, QWidget *parent = nullptr);

public slots:
    void SLOT_SaveMap();
    void SLOT_MapSizeChange(int line, int column);
    void SLOT_SaveMapSize(int line, int column);

private:
    void CreateLayer(void);
    void GetInformationOfProject();
    void CreateTextureProject();
    void CreateConnection();

protected:
    void closeEvent(QCloseEvent *event) override;


private:
    QString m_nameProject;
    QJsonObject m_objectJson;
    SFMLWidget *m_sfmlWidget;
    QGridLayout *m_layout;
    EditorControl *m_controlWidget;
    MapProjectManagement *m_mapManagement;
};

#endif // EDITORWINDOW_H
