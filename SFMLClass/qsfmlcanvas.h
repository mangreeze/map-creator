#ifndef QSFMLCANVAS_H
#define QSFMLCANVAS_H

#include <QWidget>
#include <SFML/Graphics.hpp>
#include <QTimer>
#include <QResizeEvent>

class QSFMLCanvas : public QWidget, public sf::RenderWindow
{
    Q_OBJECT

public:
    QSFMLCanvas(QWidget *parent, unsigned int frameTime = 0);
    ~QSFMLCanvas() {};

public:
    virtual void OnInit() = 0;
    virtual void OnUpdate() = 0;
    virtual QPaintEngine* paintEngine() const;
    virtual void showEvent(QShowEvent *event);
    virtual void paintEvent(QPaintEvent *event);
    virtual QSize sizeHint() const;
    virtual QSize minimumSizeHint() const;

private:
    sf::View m_view;
    QTimer m_timer;
    bool m_initialized;
};

#endif // QSFMLCANVAS_H
