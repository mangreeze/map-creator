#include <QDebug>
#include "graphics.h"

Graphics::Graphics()
{
    m_currentTexture = 0;
    m_currentHitbox = 0;
    m_currentVertices = VERTICES_TEXTURE;
}

void Graphics::createOneVerticeHitbox(const int x, const int y, const int hint, const int width, const sf::Vector2i startView, sf::VertexArray *array)
{
    sf::Color color;
    if (hint == 0) {
        color = sf::Color(192, 192, 192);
    } else if (hint == 1) {
        color = sf::Color::Red;
    } else {
        color = sf::Color::Green;
    }
    sf::Vertex *quad = &(*array)[(x + y * width) * 4];

    quad[0].position = sf::Vector2f(m_sizeTile.width() * (x + startView.x), m_sizeTile.height() * (y + startView.y));
    quad[1].position = sf::Vector2f(m_sizeTile.width() * (x + startView.x + 1), m_sizeTile.height() * (y + startView.y));
    quad[2].position = sf::Vector2f(m_sizeTile.width() * (x + startView.x + 1), m_sizeTile.height() * (y + startView.y + 1));
    quad[3].position = sf::Vector2f(m_sizeTile.width() * (x + startView.x), m_sizeTile.height() * (y + startView.y + 1));
    quad[0].color = color;
    quad[1].color = color;
    quad[2].color = color;
    quad[3].color = color;
}

void Graphics::createOneVerticeTexture(const int x, const int y, const int hint, const int width, const sf::Vector2i startView, sf::VertexArray *array)
{
    int line = hint / m_nbrTextureByLine;
    int col = hint % m_nbrTextureByLine;
    sf::Vertex *quad = &(*array)[(x + y * width) * 4];

    quad[0].position = sf::Vector2f(m_sizeTile.width() * (x + startView.x), m_sizeTile.height() * (y + startView.y));
    quad[1].position = sf::Vector2f(m_sizeTile.width() * (x + startView.x + 1), m_sizeTile.height() * (y + startView.y));
    quad[2].position = sf::Vector2f(m_sizeTile.width() * (x + startView.x + 1), m_sizeTile.height() * (y + startView.y + 1));
    quad[3].position = sf::Vector2f(m_sizeTile.width() * (x + startView.x), m_sizeTile.height() * (y + startView.y + 1));
    quad[0].texCoords = sf::Vector2f(col * m_sizeTile.width() + 1, line * m_sizeTile.height() + 1);
    quad[1].texCoords = sf::Vector2f((col + 1) * m_sizeTile.width() - 1, line * m_sizeTile.height() + 1);
    quad[2].texCoords = sf::Vector2f((col + 1) * m_sizeTile.width() - 1, (line + 1) * m_sizeTile.height() - 1);
    quad[3].texCoords = sf::Vector2f(col * m_sizeTile.width() + 1, (line + 1) * m_sizeTile.height() - 1);
}

void Graphics::ChangeOneVerticeTexture(const int x, const int y)
{
    sf::Vector2i startView = calculateStartView();
    sf::Vector2i endView = calculateEndView();
    int width = endView.x - startView.x;

    int line = m_currentTexture / m_nbrTextureByLine;
    int col = m_currentTexture % m_nbrTextureByLine;
    sf::Vertex *quad = &m_verticesTexture[((y - startView.x) + (x - startView.y) * width) * 4];

    quad[0].texCoords = sf::Vector2f(col * m_sizeTile.width() + 1, line * m_sizeTile.height() + 1);
    quad[1].texCoords = sf::Vector2f((col + 1) * m_sizeTile.width() - 1, line * m_sizeTile.height() + 1);
    quad[2].texCoords = sf::Vector2f((col + 1) * m_sizeTile.width() - 1, (line + 1) * m_sizeTile.height() - 1);
    quad[3].texCoords = sf::Vector2f(col * m_sizeTile.width() + 1, (line + 1) * m_sizeTile.height() - 1);
}

void Graphics::ChangeOneVerticeHitbox(const int x, const int y)
{
    sf::Color color;
    if (m_currentHitbox == 0) {
        color = sf::Color(192, 192, 192);
    } else if (m_currentHitbox == 1) {
        color = sf::Color::Red;
    } else {
        color = sf::Color::Green;
    }
    sf::Vector2i startView = calculateStartView();
    sf::Vector2i endView = calculateEndView();
    int width = endView.x - startView.x;

    sf::Vertex *quad = &m_verticesHitbox[((y - startView.x) + (x - startView.y) * width) * 4];

    quad[0].color = color;
    quad[1].color = color;
    quad[2].color = color;
    quad[3].color = color;
}

void Graphics::printOnePartOfTheGrid(sf::RenderWindow *window, int min, int max, int begin, int end, bool width)
{
    for (int ctLine = min; ctLine != max; ctLine += 1) {
        int hint = 0;
        int ct;
        int maxLine;
        if (width == true) {
            ct = begin * m_sizeTile.width();
            maxLine = end * m_sizeTile.width();
        } else {
            ct = begin * m_sizeTile.height();
            maxLine = end * m_sizeTile.height();
        }
        while (ct < maxLine) {
            if (hint % 2 == 0 && width == false) {
                sf::Vertex line[] = {
                    sf::Vertex(sf::Vector2f((float)ctLine * m_sizeTile.width(),  (float)ct), sf::Color::Black),
                    sf::Vertex(sf::Vector2f((float)ctLine * m_sizeTile.width(), (float)(ct + 3)), sf::Color::Black)
                };
                window->draw(line, 2, sf::Lines);
            } else if (hint % 2 == 0 && width == true) {
                sf::Vertex line[] = {
                    sf::Vertex(sf::Vector2f((float)ct, (float)ctLine * m_sizeTile.height()), sf::Color::Black),
                    sf::Vertex(sf::Vector2f((float)(ct + 3), (float)ctLine * m_sizeTile.height()), sf::Color::Black)
                };
                window->draw(line, 2, sf::Lines);
            }
            ct += 3;
            hint = (hint + 1) % 2;
        }
    }
}

void Graphics::printGrid(sf::RenderWindow *window)
{
    sf::Vector2i start = calculateStartView();
    sf::Vector2i end = calculateEndView();

    this->printOnePartOfTheGrid(window, start.y, end.y, start.x, end.x, true);
    this->printOnePartOfTheGrid(window, start.x, end.x, start.y, end.y, false);
}

void Graphics::createVerticeTexture()
{
    sf::VertexArray array;
    array.setPrimitiveType(sf::Quads);
    sf::Vector2i startView = calculateStartView();
    sf::Vector2i endView = calculateEndView();
    int width = endView.x - startView.x;
    int height = endView.y - startView.y;
    array.resize(width * height * 4);
    for (int y = 0; y != width; y++) {
        for (int x = 0; x != height; x++) {
            this->createOneVerticeTexture(y, x, m_mapManagement->GetHintTileTexture(x + startView.y, y + startView.x), width, startView, &array);
        }
    }
    m_verticesTexture.clear();
    m_verticesTexture = array;
}

void Graphics::createVerticeHitbox()
{
    sf::VertexArray array;
    array.setPrimitiveType(sf::Quads);
    sf::Vector2i startView = calculateStartView();
    sf::Vector2i endView = calculateEndView();
    int width = endView.x - startView.x;
    int height = endView.y - startView.y;
    array.resize(width * height * 4);
    for (int y = 0; y != width; y++) {
        for (int x = 0; x != height; x++) {
            this->createOneVerticeHitbox(y, x, m_mapManagement->GetHintTileHitbox(x + startView.y, y + startView.x), width, startView, &array);
        }
    }
    m_verticesHitbox.clear();
    m_verticesHitbox = array;
}

void Graphics::CreateVertices()
{
    createVerticeTexture();
    createVerticeHitbox();
}

void Graphics::DrawVertices(sf::RenderWindow *window)
{
    if (m_currentVertices == VERTICES_TEXTURE)
        window->draw(m_verticesTexture, &m_textureProject);
    else
        window->draw(m_verticesHitbox);
    this->printGrid(window);
}

void Graphics::SetTextureProject(QString fileName, int nbrTextureByLine)
{
    m_textureProject.loadFromFile(fileName.toStdString());
    m_nbrTextureByLine = nbrTextureByLine;
}

void Graphics::SetInformationOfMap(QSize sizeTile, QSize sizeMap)
{
    m_sizeMap = sizeMap;
    m_sizeTile = sizeTile;
}

void Graphics::setInfoWindow(sf::View view, sf::Vector2u sizeWindow)
{
    m_view = view;
    m_sizeWindow = sizeWindow;
}

void Graphics::SetOneTileTexture(sf::Vector2i posMouse)
{
    sf::Vector2i posTile;

    CalculatePositionInGrid(posMouse, &posTile);
    if (posTile.x < 0 || posTile.x >= m_sizeMap.height() || posTile.y < 0 || posTile.y >= m_sizeMap.width())
        return;
    if (m_currentVertices == VERTICES_TEXTURE) {
        m_mapManagement->ModifyMapTexture(posTile.y, posTile.x, m_currentTexture);
        ChangeOneVerticeTexture(posTile.y, posTile.x);
    } else {
        m_mapManagement->ModifyMapHitbox(posTile.y, posTile.x, m_currentHitbox);
        ChangeOneVerticeHitbox(posTile.y, posTile.x);
    }
}

void Graphics::CalculatePositionInGrid(sf::Vector2i pos, sf::Vector2i *hint)
{
    hint->x = (int)(((pos.x * m_view.getSize().x / m_sizeWindow.x) + (m_view.getCenter().x - (m_view.getSize().x / 2))) / m_sizeTile.width());
    hint->y = (int)(((pos.y * m_view.getSize().y / m_sizeWindow.y) + (m_view.getCenter().y - (m_view.getSize().y / 2))) / m_sizeTile.height());
}

sf::Vector2i  Graphics::calculateStartView()
{
    sf::Vector2i result;

    result.x = (m_view.getCenter().x - (m_view.getSize().x / 2)) / m_sizeTile.width();
    result.y = (m_view.getCenter().y - (m_view.getSize().y / 2)) / m_sizeTile.height();

    if (result.x != 0) {
        result.x -= 1;
    }
    if (result.y != 0) {
        result.y -= 1;
    }
    return result;
}

sf::Vector2i  Graphics::calculateEndView()
{
    sf::Vector2i result;

    result.x = (m_view.getCenter().x + (m_view.getSize().x / 2)) / m_sizeTile.width();
    result.y = (m_view.getCenter().y + (m_view.getSize().y / 2)) / m_sizeTile.height();
    if (result.x != m_sizeMap.height()) {
        result.x += 1;
    }
    if (result.y != m_sizeMap.width()) {
        result.y += 1;
    }
    if (m_sizeTile.width() * m_sizeMap.height() < m_view.getCenter().x + (m_view.getSize().x / 2)) {
        result.x = m_sizeMap.height();
    }
    if (m_sizeTile.height() * m_sizeMap.width() < m_view.getCenter().y + (m_view.getSize().y / 2)) {
        result.y = m_sizeMap.width();
    }
    return result;
}

void Graphics::ChangeCurrentTexture(int newHint)
{
    m_currentTexture = newHint;
}

void Graphics::SetSizeWindow(sf::Vector2u newSize)
{
    m_sizeWindow = newSize;
}

sf::Vector2f Graphics::CalculateSizePixelMap()
{
    sf::Vector2f size;
    size.x = m_sizeMap.width() * m_sizeTile.width();
    size.y = m_sizeMap.height() * m_sizeTile.height();
    return size;
}

void Graphics::SetMapManagement(MapProjectManagement *ptr)
{
    m_mapManagement = ptr;
}

sf::Vector2u Graphics::GetSizeWindow()
{
    return m_sizeWindow;
}

void Graphics::SetView(sf::View newView)
{
    m_view = newView;
}

void Graphics::SetCurrentVertices(const bool hint)
{
    m_currentVertices = hint;
}

void Graphics::SLOT_ChangeCurrentVertices(int hint)
{
    if (hint == 0)
        m_currentVertices = VERTICES_TEXTURE;
    else
        m_currentVertices = VERTICES_HITBOX;
}

void Graphics::SLOT_ChangeCurrentHintHitbox(int hint)
{
    m_currentHitbox = hint;
}
