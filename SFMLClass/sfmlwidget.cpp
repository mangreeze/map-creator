#include <iostream>
#include <QDebug>
#include "sfmlWidget.h"
#include <SFML/Graphics.hpp>


SFMLWidget::SFMLWidget(QWidget *parent)
    :   QSFMLCanvas(parent)
{
    m_graphics = new Graphics();
    m_mapMoveControl = new MapMoveControl(this, m_graphics, &m_view);
}

void SFMLWidget::OnInit()
{
    m_view.reset(sf::FloatRect(0, 0, 314, 314));
    setView(m_view);
    m_graphics->setInfoWindow(m_view, getSize());
    CreateVertice();
}

void SFMLWidget::OnUpdate()
{
    sf::Event event;

    while (this->pollEvent(event)) {
        if (event.type == sf::Event::Resized) {
            sf::Vector2u sizeOldWindow = m_graphics->GetSizeWindow();
            sf::FloatRect visibleArea;
            visibleArea.left = m_view.getCenter().x - (m_view.getSize().x / 2);
            visibleArea.top = m_view.getCenter().y - (m_view.getSize().y / 2);
            if ((int)event.size.width - (int)sizeOldWindow.x < 0) {
                visibleArea.width = m_view.getSize().x - ((sizeOldWindow.x - event.size.width) * m_view.getSize().x / sizeOldWindow.x);
            } else {
                visibleArea.width = m_view.getSize().x + ((event.size.width - sizeOldWindow.x) * m_view.getSize().x / sizeOldWindow.x);
            }
            if ((int)event.size.height - (int)sizeOldWindow.y < 0) {
                visibleArea.height = m_view.getSize().y - ((sizeOldWindow.y - event.size.height) * m_view.getSize().y / sizeOldWindow.y);
            } else {
                visibleArea.height = m_view.getSize().y + ((event.size.height - sizeOldWindow.y) * m_view.getSize().y / sizeOldWindow.y);
            }
            m_view.reset(visibleArea);
            setView(m_view);
            m_graphics->SetView(m_view);
            m_graphics->SetSizeWindow(getSize());
            m_graphics->CreateVertices();
        }
    }
    m_mapMoveControl->LookEvent();
    sf::RenderWindow::clear(sf::Color(0, 0, 0));
    m_graphics->DrawVertices(this);
}

void SFMLWidget::SetTextureProject(QString fileName, int nbrTextureByLine)
{
    m_graphics->SetTextureProject(fileName, nbrTextureByLine);
}

void SFMLWidget::SetInformationOfMap(QSize sizeTile, QSize sizeMap)
{
    m_graphics->SetInformationOfMap(sizeTile, sizeMap);
}

void SFMLWidget::CreateVertice()
{
    m_graphics->CreateVertices();
}

void SFMLWidget::ChangeCurrentTexture(int newHint)
{
    m_graphics->ChangeCurrentTexture(newHint);
}

void SFMLWidget::MoveView(float x, float y)
{
    m_view.move(x, y);
    setView(m_view);
}

void SFMLWidget::SetMapManagement(MapProjectManagement *ptr)
{
    m_graphics->SetMapManagement(ptr);
}

Graphics *SFMLWidget::GetGraphics()
{
    return m_graphics;
}
