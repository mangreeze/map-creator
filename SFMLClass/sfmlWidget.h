#ifndef SFMLWIDGET_H
#define SFMLWIDGET_H

#include <SFML/Graphics.hpp>
#include "qsfmlcanvas.h"
#include "graphics.h"
#include "MapMoveControl.h"
#include "MapProjectManagement.h"

class SFMLWidget : public QSFMLCanvas
{
    Q_OBJECT
public:
    SFMLWidget(QWidget *parent = nullptr);
    ~SFMLWidget() {}

public:
    void OnInit();
    void OnUpdate();
    void SetTextureProject(QString fileName, int nbrTextureByLine);
    void SetInformationOfMap(QSize sizeTile, QSize sizeMap);
    void CreateVertice();
    void MoveView(float x, float y);
    void SetMapManagement(MapProjectManagement *);
    Graphics *GetGraphics();

public slots:
    void ChangeCurrentTexture(int newHint);

private:
    sf::Texture texture;
    sf::Sprite sprite;
    Graphics *m_graphics;
    sf::View m_view;
    MapMoveControl *m_mapMoveControl;
};

#endif // SFMLWIDGET_H
