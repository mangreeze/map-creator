#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <QObject>
#include <QString>
#include <QSize>
#include <SFML/Graphics.hpp>
#include "MapProjectManagement.h"

#define VERTICES_TEXTURE (false)
#define VERTICES_HITBOX (true)

class Graphics : public QObject
{
    Q_OBJECT

public:
    Graphics();

    void CreateVertices();
    void DrawVertices(sf::RenderWindow *window);
    void SetTextureProject(QString fileName, int nbrTextureByLine);
    void SetInformationOfMap(QSize sizeTile, QSize sizeMap);
    void setInfoWindow(sf::View view, sf::Vector2u sizeWindow);
    void CalculatePositionInGrid(sf::Vector2i pos, sf::Vector2i *hint);
    void ChangeCurrentTexture(int newHint);
    void SetOneTileTexture(sf::Vector2i pos);
    void SetSizeWindow(sf::Vector2u);
    sf::Vector2f CalculateSizePixelMap();
    void SetMapManagement(MapProjectManagement *ptr);
    sf::Vector2u GetSizeWindow();
    void SetView(sf::View);
    void SetCurrentVertices(const bool);

public slots:
    void SLOT_ChangeCurrentVertices(int);
    void SLOT_ChangeCurrentHintHitbox(int);

private:
    void createOneVerticeTexture(const int x, const int y, const int hint, const int width, const sf::Vector2i startView, sf::VertexArray *array);
    void printOnePartOfTheGrid(sf::RenderWindow *window, int min, int max, int begin, int end, bool width);
    void printGrid(sf::RenderWindow *window);
    sf::Vector2i calculateStartView();
    sf::Vector2i calculateEndView();
    void ChangeOneVerticeTexture(const int x, const int y);
    void ChangeOneVerticeHitbox(const int x, const int y);
    void createVerticeTexture();
    void createVerticeHitbox();
    void createOneVerticeHitbox(const int x, const int y, const int hint, const int width, const sf::Vector2i startView, sf::VertexArray *array);

private:
    sf::VertexArray m_verticesTexture;
    sf::VertexArray m_verticesHitbox;
    int m_nbrTextureByLine;
    bool m_currentVertices;
    QSize m_sizeTile;
    QSize m_sizeMap;
    sf::View m_view;
    sf::Vector2u m_sizeWindow;
    sf::Texture m_textureProject;
    int m_currentTexture;
    int m_currentHitbox;
    MapProjectManagement *m_mapManagement;
};

#endif // GRAPHICS_H
