#ifndef SFMLWIDGET_H
#define SFMLWIDGET_H

#include "qsfmlcanvas.h"
#include <SFML/Graphics.hpp>

class SFMLWidget : public QSFMLCanvas
{
public:
    SFMLWidget(QWidget *parent, const QPoint &position, const QSize &size);
    ~SFMLWidget() {}

public:
    void OnInit();
    void OnUpdate();
};

#endif // SFMLWIDGET_H
