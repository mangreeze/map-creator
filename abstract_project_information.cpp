#include <QLabel>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QDebug>
#include "abstract_project_information.h"

Abstract_project_information::Abstract_project_information(QWidget *parent)
    : QWidget(parent)
{
    CreateLayoutInformationProject();
    CreateLayoutWidgetTexture();
    CreateLayoutValidateCancelButton();
    CreateLayoutWindow();
}

Abstract_project_information::~Abstract_project_information()
{}

void Abstract_project_information::CreateLayoutInformationProject()
{
    QLabel *labelProjectName = new QLabel("Nom du projet : ");
    QLabel *labelNumLine = new QLabel("Nombre de ligne : ");
    QLabel *labelNumColumn = new QLabel("Nombre de colonne : ");
    QLabel *labelPixelLine = new QLabel("Nbr de pixel (ligne) : ");
    QLabel *labelPixelColumn = new QLabel("Nbr de pixel (colonne) : ");
    m_layoutProjectName = new QGridLayout();
    m_projectName = new QLineEdit();
    m_nbrLine = new QSpinBox();
    m_nbrColumn = new QSpinBox();
    m_pixelLine = new QSpinBox();
    m_pixelColumn = new QSpinBox();
    m_nbrLine->setMinimum(1);
    m_nbrLine->setMaximum(10000);
    m_nbrColumn->setMinimum(1);
    m_nbrColumn->setMaximum(10000);
    m_pixelLine->setValue(15);
    m_pixelColumn->setValue(15);
    m_pixelLine->setMinimum(1);
    m_pixelColumn->setMinimum(1);
    m_layoutProjectName->addWidget(labelProjectName, 0, 0);
    m_layoutProjectName->addWidget(m_projectName, 0, 1);
    m_layoutProjectName->addWidget(labelNumLine, 1, 0);
    m_layoutProjectName->addWidget(m_nbrLine, 1, 1);
    m_layoutProjectName->addWidget(labelNumColumn, 2, 0);
    m_layoutProjectName->addWidget(m_nbrColumn, 2, 1);
    m_layoutProjectName->addWidget(labelPixelLine, 3, 0);
    m_layoutProjectName->addWidget(m_pixelLine, 3, 1);
    m_layoutProjectName->addWidget(labelPixelColumn, 4, 0);
    m_layoutProjectName->addWidget(m_pixelColumn, 4, 1);
    m_layoutProjectName->setContentsMargins(11, 11, 11, 25);
}

void Abstract_project_information::CreateLayoutWidgetTexture()
{
    m_buttonAddTexture = new QPushButton("Ajouter Texture");
    m_buttonEraseTexture = new QPushButton("Effacer Texture");
    QObject::connect(m_buttonAddTexture, SIGNAL(clicked()), this, SLOT(CreateWindowAddTexture()));
    QObject::connect(m_buttonEraseTexture, SIGNAL(clicked()), this, SLOT(EraseTexture()));
    m_listWidgetTexture = new QListWidget();
    m_layoutTexture = new QGridLayout();
    m_layoutTexture->addWidget(m_buttonAddTexture, 0, 0);
    m_layoutTexture->addWidget(m_buttonEraseTexture, 0, 1);
    m_layoutTexture->addWidget(m_listWidgetTexture, 1, 0, 1, 2);
    m_layoutTexture->setContentsMargins(11, 11, 11, 25);

}

void Abstract_project_information::CreateLayoutWindow()
{
    m_layoutWindow = new QVBoxLayout();
    m_layoutWindow->addLayout(m_layoutProjectName);
    m_layoutWindow->addLayout(m_layoutTexture);
    m_layoutWindow->addLayout(m_layoutButtonCancelValidate);
    this->setLayout(m_layoutWindow);
}

void Abstract_project_information::CreateLayoutValidateCancelButton()
{
    m_buttonValidate = new QPushButton("Valider");
    m_buttonCancel = new QPushButton("Annuler");
    QObject::connect(m_buttonValidate, SIGNAL(clicked()), this, SLOT(PressButtonValidate()));
    QObject::connect(m_buttonCancel, SIGNAL(clicked()), this, SLOT(PressButtonCancel()));
    m_layoutButtonCancelValidate = new QHBoxLayout();
    m_layoutButtonCancelValidate->addWidget(m_buttonValidate);
    m_layoutButtonCancelValidate->addWidget(m_buttonCancel);
}

void Abstract_project_information::PressButtonCancel()
{
    emit SIG_returnMainLoop();
}



