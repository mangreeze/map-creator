#include "MapSizeControl.h"
#include "MapProjectManagement.h"

MapSizeControl::MapSizeControl(QWidget *parent)
    :   QWidget(parent)
{
    this->createAllButton();
}

void MapSizeControl::createAllButton()
{   
    m_eraseLineButton = new QPushButton("-");
    m_addColumnButton = new QPushButton("+");
    m_addLineButton = new QPushButton("+");
    m_eraseColumnButton = new QPushButton("-");
    QLabel *line = new QLabel("Line : ");
    QLabel *column = new QLabel("Column : ");

    m_layout = new QGridLayout();
    m_layout->addWidget(line, 0, 0, 1, 3);
    m_layout->addWidget(m_eraseLineButton, 0, 4);
    m_layout->addWidget(m_addLineButton, 0, 5);
    m_layout->addWidget(column, 1, 0, 1, 3);
    m_layout->addWidget(m_eraseColumnButton, 1, 4);
    m_layout->addWidget(m_addColumnButton, 1, 5);

    this->setLayout(m_layout);
}

QPushButton *MapSizeControl::GetAddLineButton()
{
    return m_addLineButton;
}

QPushButton *MapSizeControl::GetEraseLineButton()
{
    return m_eraseLineButton;
}

QPushButton *MapSizeControl::GetAddColumnButton()
{
    return m_addColumnButton;
}

QPushButton *MapSizeControl::GetEraseColumnButton()
{
    return m_eraseColumnButton;
}
