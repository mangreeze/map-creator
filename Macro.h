#ifndef MACRO_H
#define MACRO_H

#define PROJECT_NAME ("Name")
#define PROJECT_HINT ("Hint")
#define PROJECT_NBR_LINE ("NbrLineCell")
#define PROJECT_NBR_COLUMN ("NbrColumnCell")
#define PROJECT_PIXEL_LINE ("PixelLineCell")
#define PROJECT_PIXEL_COLUMN ("PixelColumnCell")
#define PROJECT_TEXTURE ("Texture")
#define NEW_PROJECT ("Nouveau Projet")

#endif // MACRO_H
