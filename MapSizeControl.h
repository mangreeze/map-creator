#ifndef MAPCONTROL_H
#define MAPCONTROL_H

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>

class MapSizeControl : public QWidget
{
    Q_OBJECT

public:
    MapSizeControl(QWidget *parent = nullptr);
    QPushButton *GetEraseColumnButton();
    QPushButton *GetAddColumnButton();
    QPushButton *GetEraseLineButton();
    QPushButton *GetAddLineButton();


private:
    void createAllButton();

private:
    QPushButton *m_eraseColumnButton;
    QPushButton *m_addColumnButton;
    QPushButton *m_eraseLineButton;
    QPushButton *m_addLineButton;
    QPushButton *m_lockButton;

    QGridLayout *m_layout;

};

#endif // MAPCONTROL_H
