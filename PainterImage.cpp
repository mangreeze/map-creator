#include <QtMath>
#include "PainterImage.h"
#include "Macro.h"

PainterImage::PainterImage(QJsonArray array, int nbrPixelLine, int nbrPixelColumn)
    :   m_array(array), m_pixelTile(nbrPixelLine, nbrPixelColumn)
{
    CreateImage();
}


void PainterImage::CreateImage()
{
    m_nbrTileImage = qFloor(qSqrt(m_array.size())) + 1;
    m_image = new QImage(m_pixelTile.width() * m_nbrTileImage, m_pixelTile.height() * m_nbrTileImage, QImage::Format_RGB32);
    m_painter = new QPainter(m_image);
    for (int ct = 0; ct != m_array.size(); ct++) {
        int hint = m_array.at(ct)[PROJECT_HINT].toInt();
        QImage *imageTmp = new QImage(m_array.at(ct)[PROJECT_NAME].toString());
        imageTmp->scaled(m_pixelTile.width(), m_pixelTile.height(), Qt::IgnoreAspectRatio);
        QRect rect(hint % m_nbrTileImage * m_pixelTile.width(), hint / m_nbrTileImage * m_pixelTile.height(), m_pixelTile.width(), m_pixelTile.height());
        m_painter->drawImage(rect, *imageTmp);
    }
}

void PainterImage::SaveImage(QString fileName)
{
    m_image->save(fileName);
}

QImage *PainterImage::GetImage()
{
    return m_image;
}

int PainterImage::GetNbrTextureByLine()
{
    return m_nbrTileImage;
}
