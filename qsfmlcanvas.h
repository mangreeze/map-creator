#ifndef QSFMLCANVAS_H
#define QSFMLCANVAS_H

#ifdef Q_WS_X11
#include <Qt/qx11info_x11.h>
#include <X11/Xlib.h>
#endif

#include <QWidget>
#include <SFML/Graphics.hpp>
#include <QTimer>

class QSFMLCanvas : public QWidget, public sf::RenderWindow
{
    Q_OBJECT

public:
    QSFMLCanvas(QWidget *parent, const QPoint &position, const QSize &size, unsigned int frameTime = 0);
    ~QSFMLCanvas() {};

public:
    virtual void OnInit() {};
    virtual void OnUpdate() {};
    virtual QPaintEngine* paintEngine() const;
    virtual void showEvent(QShowEvent *event);
    virtual void paintEvent(QPaintEvent *event);

private:
    QTimer m_timer;
    bool m_initialized;
};

#endif // QSFMLCANVAS_H
