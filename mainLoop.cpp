#include "mainLoop.h"

MainLoop::MainLoop()
    : QObject()
{
    m_widget = new Menu();
    connect(m_widget, SIGNAL(SIG_CreateNewProject()), this, SLOT(createWindowNewProject()));
    connect(m_widget, SIGNAL(SIG_ModifyProject(QString)), this, SLOT(createWindowModifyProject(QString)));
    connect(m_widget, SIGNAL(SIG_LaunchEditor(QString, bool)), this, SLOT(createWindowEditor(QString, bool)));
};

void MainLoop::createWindowNewProject()
{
    m_widget->deleteLater();
    m_widget = new CreateProject();
    connect(m_widget, SIGNAL(SIG_returnMainLoop()), this, SLOT(createWindowSelectionProject()));
    connect(m_widget, SIGNAL(SIG_launchEditor(QString, bool)), this, SLOT(createWindowEditor(QString, bool)));
}

void MainLoop::createWindowSelectionProject()
{
    m_widget->deleteLater();
    m_widget = new Menu();
    connect(m_widget, SIGNAL(SIG_CreateNewProject()), this, SLOT(createWindowNewProject()));
    connect(m_widget, SIGNAL(SIG_ModifyProject(QString)), this, SLOT(createWindowModifyProject(QString)));
    connect(m_widget, SIGNAL(SIG_LaunchEditor(QString, bool)), this, SLOT(createWindowEditor(QString, bool)));
}

void MainLoop::createWindowEditor(QString nameProject, bool newProject)
{
    m_widget->deleteLater();
    m_widget = new EditorWindow(nameProject, newProject);
}

void MainLoop::createWindowModifyProject(QString nameProject)
{
    m_widget->deleteLater();
    m_widget = new ModifyProject(nameProject);
    connect(m_widget, SIGNAL(SIG_returnMainLoop()), this, SLOT(createWindowSelectionProject()));
}
