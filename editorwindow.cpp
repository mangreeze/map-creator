#include <QPushButton>
#include <QApplication>
#include <QDesktopWidget>
#include <QMessageBox>
#include "Macro.h"
#include "editorwindow.h"
#include "PainterImage.h"

EditorWindow::EditorWindow(QString nameProject, bool newProject, QWidget *parent)
    : QWidget(parent), m_nameProject(nameProject)
{
    m_sfmlWidget = new SFMLWidget();
    m_controlWidget = new EditorControl();
    m_mapManagement = new MapProjectManagement();
    GetInformationOfProject();
    if (newProject) {
        m_mapManagement->CreateEmptyMap(m_objectJson[PROJECT_NBR_LINE].toInt(), m_objectJson[PROJECT_NBR_COLUMN].toInt());
        m_mapManagement->SaveArrayMap(m_nameProject);
    } else {
        m_mapManagement->FillArrayMap(m_nameProject);
    }
    CreateTextureProject();
    m_controlWidget->CreateListTexture(m_objectJson[PROJECT_TEXTURE].toArray());
    CreateConnection();
    m_sfmlWidget->SetMapManagement(m_mapManagement);
    this->CreateLayer();
}

void EditorWindow::GetInformationOfProject()
{
    QFile file("./Project/" + m_nameProject + "/Information.json");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QString data = file.readAll();
        QJsonDocument documentJson = QJsonDocument::fromJson(data.toUtf8());
        m_objectJson = documentJson.object();
        file.close();
    }
    QSize sizeTile(m_objectJson[PROJECT_PIXEL_LINE].toInt(), m_objectJson[PROJECT_PIXEL_COLUMN].toInt());
    QSize sizeMap(m_objectJson[PROJECT_NBR_LINE].toInt(), m_objectJson[PROJECT_NBR_COLUMN].toInt());
    m_sfmlWidget->SetInformationOfMap(sizeTile, sizeMap);
}

void EditorWindow::CreateLayer()
{
    m_layout = new QGridLayout;
    m_layout->addWidget(m_sfmlWidget, 0, 0, 0, 3);
    m_layout->addWidget(m_controlWidget, 0, 4);
    this->setLayout(m_layout);
    this->show();
}

void EditorWindow::CreateTextureProject()
{
    PainterImage tmp(m_objectJson[PROJECT_TEXTURE].toArray(), m_objectJson[PROJECT_PIXEL_LINE].toInt(), m_objectJson[PROJECT_PIXEL_COLUMN].toInt());
    tmp.SaveImage("./Project/" + m_nameProject + "/imageAllTexture.png");
    m_sfmlWidget->SetTextureProject("./Project/" + m_nameProject + "/imageAllTexture.png", tmp.GetNbrTextureByLine());
}

void EditorWindow::CreateConnection()
{
    connect(m_controlWidget, SIGNAL(SIG_NewHintTexture(int)), m_sfmlWidget, SLOT(ChangeCurrentTexture(int)));
    connect(m_controlWidget, SIGNAL(SIG_NewHintHitbox(int)), m_sfmlWidget->GetGraphics(), SLOT(SLOT_ChangeCurrentHintHitbox(int)));
    connect(m_controlWidget->GetSaveButton(), SIGNAL(clicked()), this, SLOT(SLOT_SaveMap()));
    connect(m_controlWidget->GetPtrMapSizeControler()->GetAddLineButton(), SIGNAL(clicked()), m_mapManagement, SLOT(SLOT_AddLine()));
    connect(m_controlWidget->GetPtrMapSizeControler()->GetEraseLineButton(), SIGNAL(clicked()), m_mapManagement, SLOT(SLOT_EraseLine()));
    connect(m_controlWidget->GetPtrMapSizeControler()->GetAddColumnButton(), SIGNAL(clicked()), m_mapManagement, SLOT(SLOT_AddColumn()));
    connect(m_controlWidget->GetPtrMapSizeControler()->GetEraseColumnButton(), SIGNAL(clicked()), m_mapManagement, SLOT(SLOT_EraseColumn()));
    connect(m_mapManagement, SIGNAL(SIG_UpdateVertices(int, int)), this, SLOT(SLOT_MapSizeChange(int, int)));
    connect(m_controlWidget->GetTabWidget(), SIGNAL(currentChanged(int)), m_sfmlWidget->GetGraphics(), SLOT(SLOT_ChangeCurrentVertices(int)));
    connect(m_mapManagement, SIGNAL(SIG_SaveSizeMap(int, int)), this, SLOT(SLOT_SaveMapSize(int, int)));
}

void EditorWindow::closeEvent(QCloseEvent *event)
{
    int answer = QMessageBox::question(this, "Information", "Voulez vous sauvegarder votre travail ?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    if (answer == QMessageBox::Yes) {
        m_mapManagement->SaveArrayMap(m_nameProject);
        event->accept();
    } else if (answer == QMessageBox::No) {
        event->accept();
    } else if (answer == QMessageBox::Cancel) {
        event->ignore();
    }
}

void EditorWindow::SLOT_SaveMap()
{
    m_mapManagement->SaveArrayMap(m_nameProject);
}

void EditorWindow::SLOT_MapSizeChange(int line, int column)
{
    QSize sizeTile(m_objectJson[PROJECT_PIXEL_LINE].toInt(), m_objectJson[PROJECT_PIXEL_COLUMN].toInt());
    QSize sizeMap(line, column);
    m_sfmlWidget->SetInformationOfMap(sizeTile, sizeMap);
    m_sfmlWidget->CreateVertice();
}

void EditorWindow::SLOT_SaveMapSize(int line, int column)
{
    QFile file("./Project/" + m_nameProject + "/Information.json");
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        m_objectJson.insert(PROJECT_NBR_LINE, QJsonValue::fromVariant(line));
        m_objectJson.insert(PROJECT_NBR_COLUMN, QJsonValue::fromVariant(column));
        QJsonDocument tmp;
        file.resize(0);
        tmp.setObject(m_objectJson);
        file.write(tmp.toJson());
        file.close();
    }
}
