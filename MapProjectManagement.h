#ifndef MAPPROJECTMANAGEMENT_H
#define MAPPROJECTMANAGEMENT_H

#include <QObject>
#include <vector>
#include <QString>
#include <fstream>
#include <iostream>

class MapProjectManagement : public QObject
{
    Q_OBJECT

public:
    MapProjectManagement();
    void FillArrayMap(const QString &fileName);
    void SaveArrayMap(const QString &fileName);
    void CreateEmptyMap(const int &width, const int &height);
    void ModifyMapTexture(const int height, const int width, const int hint);
    void ModifyMapHitbox(const int height, const int width, const int hint);
    void EraseHintTextureMap(const int texture);
    int GetHintTileTexture(const int y, const int x);
    int GetHintTileHitbox(const int y, const int x);

public slots:
    void SLOT_AddColumn();
    void SLOT_AddLine();
    void SLOT_EraseColumn();
    void SLOT_EraseLine();

private:
    void printArray();

signals:
    void SIG_UpdateVertices(int line, int column);
    void SIG_SaveSizeMap(int line, int column);

private:
    std::vector<std::vector<int> > m_arrayMapTexture;
    std::vector<std::vector<int> > m_arrayMapHitbox;
};

#endif // MAPPROJECTMANAGEMENT_H
