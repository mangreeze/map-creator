#ifndef MAPMOVECONTROL_H
#define MAPMOVECONTROL_H

#include <graphics.h>
#include <SFML/Graphics.hpp>

class MapMoveControl
{
public:
    MapMoveControl(sf::RenderWindow *, Graphics *, sf::View *);
    void LookEvent(void);

private:
    bool isMouseInWindow(sf::Vector2i posMouse);
    bool isViewNeedToMove(sf::Vector2f posToMove);

private:
    bool m_leftClick;
    sf::RenderWindow *m_sfmlWidget;
    Graphics *m_graphics;
    sf::View *m_view;
};

#endif // MAPMOVECONTROL_H
