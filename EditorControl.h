#ifndef EDITORCONTROL_H
#define EDITORCONTROL_H

#include <QPushButton>
#include <QGridLayout>
#include <QJsonArray>
#include <QListWidget>
#include <QWidget>
#include <QTabWidget>
#include "MapSizeControl.h"

class EditorControl : public QWidget
{
    Q_OBJECT

public:
    EditorControl(QWidget *parent = nullptr);
    void CreateListTexture(const QJsonArray &array);
    QListWidget *GetListTextureWidget();
    virtual QSize sizeHint() const;
    virtual QSize minimumSizeHint() const;
    QPushButton *GetSaveButton();
    MapSizeControl *GetPtrMapSizeControler();
    QTabWidget *GetTabWidget();

public slots:
    void ListTextureChange(int row);
    void SLOT_listHitboxChange(int);

signals:
    void SIG_NewHintTexture(int hint);
    void SIG_NewHintHitbox(int);

private:
    void setWindowLayout();
    void createListHitbox();

private:
    MapSizeControl *m_mapSizeWidget;
    QListWidget *m_listTexture;
    QListWidget *m_listHitbox;
    QList<int> m_hintTexture;
    QGridLayout *m_layout;
    QPushButton *m_buttonSave;
    QTabWidget *m_tabWidget;
};

#endif // EDITORCONTROL_H
