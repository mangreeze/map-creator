#ifndef PROJECT_INFORMATION_H
#define PROJECT_INFORMATION_H

#include <QWidget>
#include <QObject>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QPushButton>
#include <QList>
#include <QListWidget>
#include <QSpinBox>
#include "abstract_project_information.h"

class CreateProject : public Abstract_project_information
{
    Q_OBJECT

public:
    CreateProject(QWidget *parent = nullptr);
    ~CreateProject();

private:
    void CreateNewProject(void);
    bool CreateNewDirectoryTexture(void);
    void EraseProject(void);
    void eraseProject(QString str_nameDirectory, QList<QString> *error_file, QList<QString> *error_dir);

signals:
    void SIG_launchEditor(QString nameProject, bool newProject);

public slots:
    void EraseTexture(void) override;
    void PressButtonValidate(void) override;
    void CreateWindowAddTexture(void) override;

private:
    QList<QString> m_textureAdd;
};

#endif // PROJECT_INFORMATION_H
