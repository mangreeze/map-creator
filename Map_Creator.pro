QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    EditorControl.cpp \
    MapMoveControl.cpp \
    MapProjectManagement.cpp \
    MapSizeControl.cpp \
    PainterImage.cpp \
    SFMLClass/graphics.cpp \
    SFMLClass/qsfmlcanvas.cpp \
    SFMLClass/sfmlwidget.cpp \
    abstract_project_information.cpp \
    createProject.cpp \
    editorwindow.cpp \
    main.cpp \
    mainLoop.cpp \
    menu.cpp \
    modifyproject.cpp

HEADERS += \
    EditorControl.h \
    Macro.h \
    MapMoveControl.h \
    MapProjectManagement.h \
    MapSizeControl.h \
    PainterImage.h \
    SFMLClass/graphics.h \
    SFMLClass/qsfmlcanvas.h \
    SFMLClass/sfmlWidget.h \
    abstract_project_information.h \
    createProject.h \
    editorwindow.h \
    mainLoop.h \
    menu.h \
    modifyproject.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-main
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-main-d

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-window
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-window-d

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-audio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-audio-d

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-graphics
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-graphics-d

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-system
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../SFML-2.5.1/lib/ -lsfml-system-d

INCLUDEPATH += "./SFMLClass/"
INCLUDEPATH += $$PWD/../../../../../SFML-2.5.1/include
DEPENDPATH += $$PWD/../../../../../SFML-2.5.1/include

RESOURCES += \
    Ressource.qrc

DISTFILES += \
    Photo Hello-World [Clément Bécamel Stéphane].png
