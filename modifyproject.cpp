#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFileDialog>
#include <QMessageBox>
#include "modifyproject.h"
#include "Macro.h"
#include "MapProjectManagement.h"

ModifyProject::ModifyProject(QString nameProject, QWidget *parent)
    : Abstract_project_information(parent)
{
    m_hintAddNewTexture = 0;
    this->getJsonInformationProject(nameProject);
    this->initWindow();
    this->show();
}

void ModifyProject::getJsonInformationProject(QString nameProject)
{
    QFile file("./Project/" + nameProject + "/Information.json");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QString data = file.readAll();
        QJsonDocument documentJson = QJsonDocument::fromJson(data.toUtf8());
        m_objectJson = documentJson.object();
        file.close();
    }
}

void ModifyProject::initWindow()
{
    m_projectName->setText(m_objectJson[PROJECT_NAME].toString());
    m_nbrLine->setValue(m_objectJson[PROJECT_NBR_LINE].toInt());
    m_nbrColumn->setValue(m_objectJson[PROJECT_NBR_COLUMN].toInt());
    m_pixelLine->setValue(m_objectJson[PROJECT_PIXEL_LINE].toInt());
    m_pixelColumn->setValue(m_objectJson[PROJECT_PIXEL_COLUMN].toInt());
    m_nbrLine->setEnabled(false);
    m_nbrColumn->setEnabled(false);
    QJsonArray tmp = m_objectJson[PROJECT_TEXTURE].toArray();
    for (int ct = 1; ct != tmp.size(); ct++) {
        m_listTexture.append(tmp.at(ct)[PROJECT_NAME].toString().replace('\\', '/'));
        m_listWidgetTexture->addItem(tmp.at(ct)[PROJECT_NAME].toString().replace('\\', '/'));
    }
}

void ModifyProject::CreateWindowAddTexture()
{
    QString fichier = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Images (*.png *.gif *.jpg *.jpeg)");
    if (fichier.compare("") != 0) {
        m_listTexture.append(fichier);
        m_listWidgetTexture->addItem(fichier);
        this->show();
    }
}

void ModifyProject::EraseTexture()
{
    QList<QListWidgetItem *> textureToErase = m_listWidgetTexture->selectedItems();
    for (int ct = 0; ct != textureToErase.size(); ct++) {
        if (textureToErase[ct]->text()[0] == '.') {
            m_tmpListTextureToErase.append(textureToErase[ct]->text());
        }
        m_listTexture.removeOne(textureToErase[ct]->text());
        delete textureToErase[ct];
    }
    QJsonArray tmp = m_objectJson[PROJECT_TEXTURE].toArray();
    for (int ct = 0; ct != tmp.size(); ct++) {
        if (m_tmpListTextureToErase[m_tmpListTextureToErase.size() - 1].compare(tmp.at(ct)[PROJECT_NAME].toString()) == 0) {
            m_hintEraseTexture.push_back(tmp.at(ct)[PROJECT_HINT].toInt());
            tmp.removeAt(ct);
            break;
        }
    }
    m_objectJson.insert(PROJECT_TEXTURE, tmp);
    this->show();
}

void ModifyProject::PressButtonValidate()
{
    if (m_projectName->text() == "") {
        QMessageBox::warning(this, "Erreur titre projet", "Merci de rentrer un nom pour votre projet");
        return;
    } else if (m_listTexture.size() == 0) {
        QMessageBox::warning(this, "Erreur texture", "Merci d'ajouter au moins une texture");
        return;
    }
    if (m_projectName->text().compare(m_objectJson[PROJECT_NAME].toString()) != 0) {
        QDir dir;
        if (dir.rename("./Project/" + m_objectJson[PROJECT_NAME].toString(), "./Project/" + m_projectName->text()) == false) {
            QMessageBox::critical(this, "Doublon nom de projet", "Ce projet existe déja, veuillez modifier le titre avant de revalider");
            return;
        }
    }
    m_objectJson.insert(PROJECT_NAME, QJsonValue::fromVariant(m_projectName->text()));
    m_objectJson.insert(PROJECT_NBR_LINE, QJsonValue::fromVariant(m_nbrLine->value()));
    m_objectJson.insert(PROJECT_PIXEL_LINE, QJsonValue::fromVariant(m_pixelLine->value()));
    m_objectJson.insert(PROJECT_NBR_COLUMN, QJsonValue::fromVariant(m_nbrColumn->value()));
    m_objectJson.insert(PROJECT_PIXEL_COLUMN, QJsonValue::fromVariant(m_pixelColumn->value()));
    this->EraseFileTexture();
    this->AddTextureToDirectory();
    this->CreateQObjectTexture();
    QJsonDocument tmp;
    QFile file("./Project/" + m_projectName->text() + "/Information.json");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.resize(0);
    tmp.setObject(m_objectJson);
    file.write(tmp.toJson());
    file.close();
    EraseTextureInMap();
    emit SIG_returnMainLoop();
}

void ModifyProject::EraseFileTexture()
{
    for (int ct = 0; ct != m_tmpListTextureToErase.size(); ct++) {
        QFile file(m_tmpListTextureToErase[ct]);
        file.remove();
    }
}

bool ModifyProject::AddTextureToDirectory()
{
    QList<QString> error;
    for (int ct = 0; ct != m_listTexture.size(); ct++) {
        if (m_listTexture[ct][0] != '.') {
            QStringList tmp = m_listTexture[ct].split(QLatin1Char('/'));
            if (QFile::copy(m_listTexture[ct], "./Project/" + m_projectName->text() + "/Texture/" + tmp[tmp.size() - 1]) == false) {
                error.append(m_listTexture[ct]);
            } else {
                m_textureAdd.append("./Project\\" + m_projectName->text() + "/Texture/" + tmp[tmp.size() - 1]);
                m_hintNewTexture.append(true);
            }
        } else {
            m_textureAdd.append(m_listTexture[ct]);
            m_hintNewTexture.append(false);
        }
    }
    if (error.size() != 0) {
        QString tmp = "Impossible de copier les textures suivantes : \n";
        for (int ct = 0; ct != error.size(); ct++) {
            tmp += error[ct] + "\n";
        }
        QMessageBox::critical(this, "Erreur Texture", tmp);
        return false;
    }
    return true;
}

void ModifyProject::CreateQObjectTexture()
{
    QJsonArray texture;
    QJsonArray dataArray = m_objectJson[PROJECT_TEXTURE].toArray();

    QJsonObject tmp2;
    tmp2.insert(PROJECT_NAME, QJsonValue::fromVariant(":/Image/GreyImage.png"));
    tmp2.insert(PROJECT_HINT, QJsonValue::fromVariant(0));
    texture.append(tmp2);
    for (int ct = 0; ct != m_textureAdd.size(); ct++) {
        if (m_hintNewTexture[ct] == true) {
           this->AddNewTexture(m_textureAdd[ct], dataArray, &texture);
        } else {
            for (int ct2 = 0; ct2 != dataArray.size(); ct2++) {
                if (dataArray.at(ct2)[PROJECT_NAME].toString().replace('\\', '/').compare(m_textureAdd[ct]) == 0) {
                    QJsonObject tmp;
                    tmp.insert(PROJECT_NAME, m_textureAdd[ct]);
                    tmp.insert(PROJECT_HINT, dataArray.at(ct2)[PROJECT_HINT].toInt());
                    texture.append(tmp);
                    break;
                }
            }
        }
    }
    QJsonArray ArrayTexture;
    for (int ct = 0; ct != texture.size(); ct++) {
        bool find = false;
        for (int ct2 = 0; ct2 != ArrayTexture.size(); ct2++) {
            if (texture.at(ct)[PROJECT_HINT].toInt() < ArrayTexture.at(ct2)[PROJECT_HINT].toInt()) {
                QJsonObject tmp;
                tmp.insert(PROJECT_NAME, texture.at(ct)[PROJECT_NAME].toString());
                tmp.insert(PROJECT_HINT, texture.at(ct)[PROJECT_HINT].toInt());
                ArrayTexture.insert(ct2, tmp);
                find = true;
                break;
            }
        }
        if (find == false) {
            QJsonObject tmp;
            tmp.insert(PROJECT_NAME, texture.at(ct)[PROJECT_NAME].toString());
            tmp.insert(PROJECT_HINT, texture.at(ct)[PROJECT_HINT].toInt());
            ArrayTexture.append(tmp);
        }
    }
    m_objectJson.insert(PROJECT_TEXTURE, ArrayTexture);
}

void ModifyProject::AddNewTexture(QString fileName, QJsonArray oldData, QJsonArray *newData)
{
    bool find = false;

    while (find == false) {
        int ct = 0;
        while (ct != oldData.size()) {
            if (m_hintAddNewTexture == oldData.at(ct)[PROJECT_HINT].toInt()) {
                break;
            }
            ct += 1;
        }
        if (ct == oldData.size()) {
            QJsonObject tmp;
            tmp.insert(PROJECT_NAME, fileName.replace('\\', '/'));
            tmp.insert(PROJECT_HINT, m_hintAddNewTexture);
            newData->append(tmp);
            find = true;
        }
        m_hintAddNewTexture += 1;
    }
}

void ModifyProject::EraseTextureInMap()
{
    MapProjectManagement tmp;

    tmp.FillArrayMap(m_projectName->text());
    for (int ct = 0; ct != m_hintEraseTexture.size(); ct++) {
        tmp.EraseHintTextureMap(m_hintEraseTexture[ct]);
    }
    tmp.SaveArrayMap(m_projectName->text());
}
