L'objectif du projet est de réaliser un petit éditeur de map 2D.

Liste des fonctionnalités :
    -   Créer/Sauvegarder/Effacer un projet
    -   Sélection des données de la map
    -   Ajout de colonne et de ligne en temps réel